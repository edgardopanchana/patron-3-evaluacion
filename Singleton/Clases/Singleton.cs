﻿using System;
namespace Singleton.Clases
{
    public sealed class Singletone
    {
        private static Singletone instancia = null;

        protected Singletone()        {        }

        private Cliente cliente;        public Cliente Cliente        {            get { return cliente; }            set { cliente = value; }        }

        public static Singletone Instancia        {            get
            {                if (instancia == null)
                {
                    Console.WriteLine("Se instancia");
                    instancia = new Singletone();
                }                                    return instancia;            }

        }
        
    }
}
